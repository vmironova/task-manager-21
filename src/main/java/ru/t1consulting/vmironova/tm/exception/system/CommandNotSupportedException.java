package ru.t1consulting.vmironova.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command not supported.");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! Command '" + command + "' not supported.");
    }

}
